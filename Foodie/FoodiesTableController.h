//
//  FoodiesTableController.h
//  Foodie
//
//  Created by IT-Högskolan on 2015-02-17.
//  Copyright (c) 2015 tilo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodiesTableController : UITableViewController

- (void) setFoodiesArr: (NSMutableArray*) arrFromGetFoodies;
- (void) setFoodiesDict: (NSDictionary*)newValue;

@end
