//
//  GetFoodiesController.m
//  Foodie
//
//  Created by IT-Högskolan on 2015-02-17.
//  Copyright (c) 2015 tilo. All rights reserved.
//

#define kBgQueue dispatch_get_global_queue(
/*
DISPATCH_QUEUE_PRIORITY_DEFAULT,; 0) //1
#define kLatestKivaLoansURL [NSURL URLWithString:
@"http://api.kivaws.org/v1/loans/search.json?status=fundraising"] //2
*/

#import "GetFoodiesController.h"
#import "FoodiesList.h"
#import "FoodiesTableController.h"

@interface GetFoodiesController ()
@property (weak, nonatomic) IBOutlet UILabel *labelLoading;
@property (weak, nonatomic) IBOutlet UIImageView *viewBackground;

@property (strong, nonatomic) NSDictionary *foodiesDict;
@property (weak, nonatomic) IBOutlet UITextField *fieldSearch;
@property (weak, nonatomic) IBOutlet UITextView *textSearchResult;
@property (weak, nonatomic) IBOutlet UIButton *btnSearch;
@property (nonatomic) NSMutableArray* foodiesArr;

@property (nonatomic) UIDynamicAnimator *animator;
@property (nonatomic) UIGravityBehavior *gravity;
@property (nonatomic) UIAttachmentBehavior *attachment;
@property (nonatomic) UIDynamicItemBehavior *behavior;
@property (nonatomic) UISnapBehavior *snap;
@property (nonatomic) UICollisionBehavior *collision;



@end

UIDynamicAnimator* _animator;
UIGravityBehavior* _gravity;
UICollisionBehavior* _collision;


@implementation GetFoodiesController



- (IBAction)btnSearchPressed:(id)sender {
    
   
    NSString *searchString = [NSString stringWithFormat:@"http://www.matapi.se/foodstuff?query=%@&format=json&pretty=0", self.fieldSearch.text];
    
 //   http://www.matapi.se/foodstuff?query=44&format=json&pretty=0
    
    
//    NSString *searchString = [NSString stringWithFormat:@"http://api.duckduckgo.com/?q=%@&format=json&pretty=0",self.fieldSearch.text];
    
    NSString *safeSearchString = [searchString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"%@",safeSearchString);
 
    NSURL *url = [NSURL URLWithString: safeSearchString];
    
    NSURLRequest *request = [NSURLRequest requestWithURL: url];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        NSLog(@"Got data! Data: %@, error: %@",data,error);
        
        // Kolla om man har fått ett fel först
        
        if(error) NSLog(@"error");
 
        NSError *parseError;
        
        self.root = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&parseError];
        
        if(!parseError) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if(self.root.count > 0) {
                    
                    // Stupid array wont add anything without this line first. Initialize? 
                   self.foodiesArr = [NSMutableArray arrayWithObjects: nil];
            //eller
              //      self.foodiesArr = [@[@"My", @"Ass", @"Init", @"Array"] mutableCopy];
                    
                    for(int i = 0; i < self.root.count; i++) {
                        
                        self.foodiesArr[i] = self.root[i][@"name"];
                        
                        // the other way to add stuff to a mutable array
                     //   [self.foodiesArr addObject: self.root[i][@"name"]];
                    }
                    self.textSearchResult.text = self.root[0][@"name"]; // ändra
                
                } else {
                    self.textSearchResult.text = @"No topics found.";
                }
                
                [self performSegueWithIdentifier:@"NextController" sender:self];
            });
        } else {
            NSLog(@"Couldn't parse json: %@",parseError);
        }
        
    }];
 
    [task resume];
    [self.view endEditing:YES];

}


- (IBAction)searchAction:(id)sender {
}

- (void)viewDidLoad {
    [super viewDidLoad];
    

    
    
    [self loading];
    
}

- (void) loading {
/*
    UIView* square = [[UIView alloc] initWithFrame:
                      CGRectMake(100, 100, 100, 100)];
    square.backgroundColor = [UIColor grayColor];
    [self.view addSubview:square];
    
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    self.gravity = [[UIGravityBehavior alloc] initWithItems:@[square]];
    
    [self.animator addBehavior:self.gravity];
*/
    
    
    NSString* loading = self.labelLoading.text;
    self.labelLoading.text = [NSString stringWithFormat: @"%@ ...",loading];
    
    
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    [self.view addSubview: self.labelLoading];
    
    self.gravity = [[UIGravityBehavior alloc] initWithItems:@[self.labelLoading]];
    [self.animator addBehavior:self.gravity];
/*
    self.collision = [[UICollisionBehavior alloc] initWithItems:@[self.labelLoading, self.textSearchResult]];
    self.collision.translatesReferenceBoundsIntoBoundary = YES;
    [self.animator addBehavior:self.collision];
    
    self.snap = [[UISnapBehavior alloc] initWithItem:self.textSearchResult snapToPoint:self.textSearchResult.center];
//    [self.animator addBehavior:self.snap];
 
    self.attachment = [[UIAttachmentBehavior alloc] initWithItem:self.textSearchResult attachedToAnchor:self.view.center];
    self.attachment.length = 0.0f;
    [self.animator addBehavior:self.attachment];
    
    self.labelLoading.alpha = 0;
    [UIView animateWithDuration:1.5 delay:0.5 options:UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse animations:^{
        self.labelLoading.alpha = 1;
    } completion:nil];
    
*/
    
/*
    //    self.animator

    UIView *square1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 64, 64)];
    square1.center = self.view.center;
    square1.backgroundColor = [UIColor greenColor];
    [self.view addSubview:square1];
    
    self.gravity = [[UIGravityBehavior alloc] initWithItems:@[square1]];
    [self.animator addBehavior:self.gravity];
    
    self.collision = [[UICollisionBehavior alloc] initWithItems:@[square1, self.textSearchResult]];
    self.collision.translatesReferenceBoundsIntoBoundary = YES;
    [self.animator addBehavior:self.collision];
    
    self.snap = [[UISnapBehavior alloc] initWithItem:self.textSearchResult snapToPoint:self.textSearchResult.center];
    [self.animator addBehavior:self.snap];
    
    self.attachment = [[UIAttachmentBehavior alloc] initWithItem:self.textSearchResult attachedToAnchor:self.view.center];
    self.attachment.length = 0.0f;
    [self.animator addBehavior:self.attachment];
 
 */

    [self getFoodies];
    
}

- (void) getFoodies {


    FoodiesList *fooList = [[FoodiesList alloc] init];
    [fooList setFoodiesDict];
    
    self.foodiesDict = [fooList foodiesDict];
    
    FoodiesTableController *foo = [[FoodiesTableController alloc] init];
   
    [foo setFoodiesDict: self.foodiesDict];
    
   // NSLog(@"getFoodies called: %@", self.foodiesDict[@"name"]);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    FoodiesTableController *target = segue.destinationViewController;
  //  NSLog(@"prepareForSegue Called: %@", self.foodiesArr[0]);
    
    [target setFoodiesArr:self.foodiesArr];
}


@end
