//
//  FoodiesList.m
//  Foodie
//
//  Created by IT-Högskolan on 2015-02-17.
//  Copyright (c) 2015 tilo. All rights reserved.
//

#import "FoodiesList.h"


@interface FoodiesList()

@property (weak, nonatomic) NSMutableArray *foodiesArr;
@property (strong, nonatomic) NSDictionary *foodiesDict;


@end

@implementation FoodiesList


@synthesize foodiesArr  = _foodiesArr;
@synthesize foodiesDict = _foodiesDict;

-(NSMutableArray*) FoodiesArr {
    
    return _foodiesArr;
}

- (void)FoodiesDict: (NSMutableArray *)newValue {
    
    self.FoodiesArr = newValue;
    
}

- (void) setFoodiesDict {
    
    //   NSDictionary *dict = @{@"key1":@"Eezy",@"key2": @"Tutorials"};
    
    _foodiesDict =      @{@"name":   @"rågmjöl",
                          @"name":   @"vetemjöl",
                          @"name":   @"vetemjöl special",
                          @"name": @[@"Objective C", @"Unity"]};
    
    
}

- (void) setFoodiesDict: (NSDictionary*)newValue {
    
 //   NSDictionary *dict = @{@"key1":@"Eezy",@"key2": @"Tutorials"};
    
    _foodiesDict = newValue;
    
    _foodiesDict =      @{@"name":  @"rågmjöl",
                         @"name":   @"rågmjöl",
                         @"name":   @"vetemjöl special",
                         @"name": @[@"Objective C", @"Unity"]};
    
    _foodiesDict = newValue;
    
 //   NSLog(@"Foodlist.setFoodiesDict: %@", self.foodiesDict[@"name"]);
    
}

- (NSDictionary *) foodiesDict {
    

 //   NSLog(@"Foodlist.foodiesDict: %@", _foodiesDict[@"name"]);


    return _foodiesDict;
}


@end
